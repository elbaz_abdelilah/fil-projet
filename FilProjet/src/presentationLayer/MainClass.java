package presentationLayer;

import tokenization.LectureFichier;
import tokenization.Noeud;

public class MainClass {
	private static Noeud n1,n2,n3,n4,n5,n6,n7;
	
	public static void main(String[] args) {
		
		/*
		n1 = new Noeud('a', -1, null, n2);
		n2 = new Noeud('b', 28, n3, null);
		n3 = new Noeud('c', 32, n4, n5);
		n4 = new Noeud('d', 33, null, null);
		n5 = new Noeud('a', 29, n6, null);
		n6 = new Noeud('c', 30, n7, null);
		n7 = new Noeud('e', 33, null, null);
		*/
		
		n1 = Noeud.nouveauNoeuds('a', -1);
		n2 = Noeud.nouveauNoeuds('b', -1);
		n3 = Noeud.nouveauNoeuds('c', 25);
		n1.rajouteFils(n2);
		n2.rajouteFils(n3);
		
		String mot = "bc";
		System.out.println(Noeud.recherche(n2, mot.toCharArray(), 0));
		//new LectureFichier().readFile("lexique.txt");
	}

}
