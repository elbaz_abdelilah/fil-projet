package tokenization;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class LectureFichier {

	public void readFile(String path){
	try{
	InputStream flux=new FileInputStream(path); 
	InputStreamReader lecture=new InputStreamReader(flux);
	BufferedReader buff=new BufferedReader(lecture);
	String ligne;
	StringTokenizer st;
	String[] resultSplit;
	while ((ligne=buff.readLine())!=null){
		resultSplit = ligne.split("\\s");
		System.out.println("Code : "+resultSplit[0] + "     Mot : "+resultSplit[1]);
		char[] c = resultSplit[1].toCharArray();
		
		for(Character caractere : c){
			System.out.println(caractere);
		}
		/*st = new StringTokenizer(ligne);
		while (st.hasMoreTokens()) {
		    System.out.println(st.nextToken());
		}*/
		//System.out.println(ligne);
	}
	buff.close(); 
	}		
	catch (Exception e){
	System.out.println(e.toString());
	}
	}
}

