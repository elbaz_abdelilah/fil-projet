package tokenization;

public class Noeud {
	private char c;
	private int codeMot;
	private Noeud frere;
	private Noeud fils;
	
	
	
	private Noeud(char c, int codeMot) {
		super();
		this.c = c;
		this.codeMot = codeMot;
		this.frere = null;
		this.fils = null;
	}


	public static Noeud nouveauNoeuds(char c,int code){
		return new Noeud(c, code);
	}
	
	public void rajouteFils(Noeud fils){
		this.fils = fils;
	}
	
	public void rajouteFrere(Noeud frere){
		this.frere = frere;
	}
	
	
	
	public static int recherche(Noeud n, char[] mot,int i){
		if(n==null)
			return -1;
		if(mot[i] == n.getC()){
			System.out.println("i :"+i);
			if(mot.length == i+1)
				return n.getCodeMot();
			else
				return recherche(n.getFils(), mot, i+1);
		}
		return recherche(n.getFrere(), mot, i);
	}
	
	public char getC() {
		return c;
	}


	public void setC(char c) {
		this.c = c;
	}


	public int getCodeMot() {
		return codeMot;
	}


	public void setCodeMot(int codeMot) {
		this.codeMot = codeMot;
	}


	public Noeud getFrere() {
		return frere;
	}


	public void setFrere(Noeud frere) {
		this.frere = frere;
	}


	public Noeud getFils() {
		return fils;
	}


	public void setFils(Noeud fils) {
		this.fils = fils;
	}
	
	
	
	
}
